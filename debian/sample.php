<?

//  Copyright (c) 2008  Joey Schulze <joey@infodrom.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require_once 'Crypt/GPG.php';

function gpg_create($info)
{
  if (!array_key_exists('name', $info) || !array_key_exists('pass', $info))
    return false;
  
  if (!array_key_exists('type', $info)) $info['type'] = 'RSA';
  if (!array_key_exists('length', $info)) $info['length'] = 2048;
  if (!array_key_exists('sublength', $info)) $info['sublength'] = 2048;
  if (!array_key_exists('mail', $info)) $info['mail'] = 'root@localhost';

  if (($tmpname = tempnam('/tmp', 'gpg')) === false)
    return false;

  if (($output = tempnam('/tmp', 'gpg')) === false)
    return false;

  if (($f = fopen($tmpname, "w")) === false) {
    unlink($tmpname);
    return false;
  }

  fwrite($f, sprintf("Key-Type: %s\n", $info['type']));
  fwrite($f, sprintf("Key-Length: %d\n", $info['length']));
  fwrite($f, "Subkey-Type: ELG-E\n");
  fwrite($f, sprintf("Subkey-Length: %d\n", $info['sublength']));
  fwrite($f, sprintf("Name-Real: %s\n", $info['name']));
  fwrite($f, sprintf("Name-Email: %s\n", $info['mail']));
  fwrite($f, "Expire-Date: 0\n");
  fwrite($f, sprintf("Passphrase: %s\n", $info['pass']));
  fwrite($f, "%commit\n");

  fclose($f);

  if (system("gpg --batch --gen-key $tmpname > /dev/null 2> $output") === false) {
    unlink($tmpname);
    unlink($output);
    return false;
  }

  if (($f = fopen($output, "r")) === false) {
    unlink($tmpname);
    unlink($output);
    return false;
  }

  $key = '';
  while (!feof($f)) {
    $line = fgets($f, 500);
    echo $line."\n";
    if (preg_match('/^gpg: key ([0-9A-F]{8}) .*/', $line, $matches) > 0) {
      $key = $matches[1];
    }
  }

  fclose($f);

  system("gpg --update-trustdb  2> /dev/null");

  unlink($tmpname);

  return $key;
}

# // Neuen Schl�ssel erzeugen
# $r = gpg_create(array('name' => 'Real Name',
# 		      'mail' => 'user@doma.in',
# 		      #'length' => 2048,
# 		      #'sublength' => 2048,
# 		      'pass' => 'Passphrase'));
#  


# // encrypt something
# $gpg = new Crypt_GPG();
# $gpg->addEncryptKey('0xB62F3700');
# $gpg->encryptFile('/tmp/OT16752740690.ps', 'OT16752740690.ps.gpg', false);

// decrypt file
$gpg = new Crypt_GPG();
$gpg->addDecryptKey('0xB62F3700', 'Passphrase');
$gpg->decryptFile('OT16752740690.ps.gpg', 'decoded.ps');

# // encrypt and decrypt text
# $clear = "Ein geheimer Text";
# printf("%s\n", $clear);
# $gpg = new Crypt_GPG();
# $gpg->addEncryptKey('0xB62F3700');
# $enc = $gpg->encrypt($clear);
# printf("%s\n", $enc);
# $gpg->addDecryptKey('0xB62F3700', 'Passphrase');
# $dec = $gpg->decrypt($enc);
# printf("%s\n", $dec);

?>
