php-crypt-gpg (1.6.4-1) unstable; urgency=medium

  * New upstream version 1.6.4

 -- Martin Dosch <martin@mdosch.de>  Mon, 31 Aug 2020 15:50:34 +0200

php-crypt-gpg (1.6.0-1) unstable; urgency=medium

  * New upstream version 1.6.0
    - Adds GnuPG 2.1 support (Closes: #835592)
  * Drop (build-)depend on gnupg (Closes: #863182)
  * refresh patch
  * Update Standards-Version to 4.0.0

 -- Prach Pongpanich <prach@debian.org>  Wed, 02 Aug 2017 12:58:39 +0700

php-crypt-gpg (1.4.1-1) unstable; urgency=medium

  * Imported Upstream version 1.4.1
  * Update Standards-Version to 3.9.8
  * Refresh patch 0001-Group-write-if-write-access-is-needed

 -- Prach Pongpanich <prach@debian.org>  Tue, 10 May 2016 13:26:17 +0700

php-crypt-gpg (1.4.0-2) unstable; urgency=medium

  * Team upload
  * Use standard gbp layout
  * PHP 7.0 transition:
    - Use now split php-mbstring for the tests
    - Rebuild with recent pkg-php-tools
  * Update Standards-Version to 3.9.7
  * Work around failing test needing write access

 -- David Prévot <taffit@debian.org>  Fri, 25 Mar 2016 14:25:09 -0400

php-crypt-gpg (1.4.0-1) unstable; urgency=medium

  [ Prach Pongpanich ]
  * Bump Standards-Version to 3.9.6
  * Run test during package build
  * Add DEP-8 tests

  [ David Prévot ]
  * Track latest release
  * Update copyright
  * Let pkg-php-tools take care of dependencies

  [ Christian Weiske ]
  * Implement #20939: Methods to retrieve status about encryption and
    signing keys
  * Implement request #20940: Information about recently created signature

  [ Aleksander Machniak ]
  * Add possibility to get/set all usage flags of the subkey
  * Return all fingerprintes from key import methods (Request #17815)
  * Fix issue where PinEntry could not find Console/CommandLine (Bug #19914)
  * Fix converting HTML entities in debug output (Bug #20512)
  * Fix path to pinentry-cli.xml file on composer installs (Bug #20527)
  * Fix key import tests after adding 'fingerprints' item to the result
  * Get rid of @-operator, better variables naming
  * Prepare 1.4.0 release

 -- Prach Pongpanich <prach@debian.org>  Tue, 24 Nov 2015 19:15:25 +0700

php-crypt-gpg (1.3.2-1) unstable; urgency=low

  * New upstream release
  * Now using PKG-PHP-PEAR team as maintainer
  * Switch to pkg-php-tools and dh-sequencer
    - Drop debian/{postinst,prerm} file (Closes: #689324)
  * Add Depends on misc-depends
  * Add myself as uploader
  * Switch VCS to git (Closes: #638505)
  * Switch to 3.0 (quilt) source format (Closes: #664382)
  * Switch debian/copyright to format 1.0
  * Bump debhelper compat to 9
  * Bump Standards-Version to 3.9.4

 -- Prach Pongpanich <prachpub@gmail.com>  Thu, 20 Jun 2013 21:15:34 +0700

php-crypt-gpg (1.1.1-1) unstable; urgency=low

  * New upstream source
  * Corrects problem with specifying trustdb

 -- Joey Schulze <joey@infodrom.org>  Sat, 19 Jun 2010 17:06:22 +0200

php-crypt-gpg (1.1.0-1) unstable; urgency=low

  * Change section from web to php to accomplish an override disparity
  * New upstream version

 -- Joey Schulze <joey@infodrom.org>  Sat, 15 May 2010 20:54:46 +0200

php-crypt-gpg (1.0.0-2) unstable; urgency=low

  * Add possibility to add --textmode to signing command line, required
    for digitally signing mail [patches/sign-textmode.dpatch], extracted
    from PEAR feature request <http://pear.php.net/bugs/bug.php?id=17006>

 -- Joey Schulze <joey@infodrom.org>  Sat, 23 Jan 2010 11:03:11 +0100

php-crypt-gpg (1.0.0-1) unstable; urgency=low

  * New upstream version
    . Includes improved subkeys handling
  * Final release 1.0.0
  * Removed subkeys patch since this problem is fixed upstream

 -- Joey Schulze <joey@infodrom.org>  Sun, 25 Jan 2009 09:39:09 +0100

php-crypt-gpg (1.0.0~RC1-3) unstable; urgency=low

  * Removed pear post-install installation call until draf PHP policy is
    fixed, this will make the package installable again

 -- Joey Schulze <joey@infodrom.org>  Tue, 13 Jan 2009 22:15:06 +0100

php-crypt-gpg (1.0.0~RC1-2) unstable; urgency=low

  * Added a simple example
  * Added calls to pear install/uninstall to the postinst/prerm script as
    per draft PHP policy, section 4.2.2

 -- Joey Schulze <joey@infodrom.org>  Wed, 07 Jan 2009 17:21:26 +0100

php-crypt-gpg (1.0.0~RC1-1) unstable; urgency=low

  * Initial packaging
  * Improved rules file by dynamically importing dpatch snippets from dpatch
  * Search for keys with passphrases not only for the particularly used
    subkey but also for the main key id in case the passphrase is stored
    under the main key id [01-not_only_subkeys.dpatch]
  * Remove executable bit for patches on clean target

 -- Joey Schulze <joey@infodrom.org>  Fri, 26 Dec 2008 20:37:24 +0100
